use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

enum PlayResult {
    Win,
    Loss,
    Tie,
}

impl PlayResult {
    fn value(&self) -> u32 {
        match self {
            Self::Win => 6,
            Self::Loss => 0,
            Self::Tie => 3,
        }
    }
}

enum Play {
    Rock,
    Paper,
    Scissors,
}

impl Play {
    fn vs(&self, other: &Self) -> PlayResult {
        match self {
            Self::Rock => match other {
                Self::Rock => PlayResult::Tie,
                Self::Paper => PlayResult::Loss,
                Self::Scissors => PlayResult::Win,
            },
            Self::Paper => match other {
                Self::Rock => PlayResult::Win,
                Self::Paper => PlayResult::Tie,
                Self::Scissors => PlayResult::Loss,
            },
            Self::Scissors => match other {
                Self::Rock => PlayResult::Loss,
                Self::Paper => PlayResult::Win,
                Self::Scissors => PlayResult::Tie,
            },
        }
    }

    fn loses_vs(&self) -> Self {
        match self {
            Self::Rock => Self::Paper,
            Self::Paper => Self::Scissors,
            Self::Scissors => Self::Rock,
        }
    }

    fn wins_vs(&self) -> Self {
        match self {
            Self::Rock => Self::Scissors,
            Self::Paper => Self::Rock,
            Self::Scissors => Self::Paper,
        }
    }

    fn value(&self) -> u32 {
        match self {
            Self::Rock => 1,
            Self::Paper => 2,
            Self::Scissors => 3,
        }
    }
}

fn parse_line_a(line: &str) -> (Play, Play) {
    let plays: Vec<&str> = line.split(' ').collect();

    let oponents_play = match plays[0] {
        "A" => Play::Rock,
        "B" => Play::Paper,
        "C" => Play::Scissors,
        _ => panic!("Cannot determine oponent's play"),
    };

    let my_play = match plays[1] {
        "X" => Play::Rock,
        "Y" => Play::Paper,
        "Z" => Play::Scissors,
        _ => panic!("Cannot determine my play"),
    };

    (my_play, oponents_play)
}

fn parse_line_b(line: &str) -> (Play, PlayResult) {
    let inputs: Vec<&str> = line.split(' ').collect();

    let oponents_play = match inputs[0] {
        "A" => Play::Rock,
        "B" => Play::Paper,
        "C" => Play::Scissors,
        _ => panic!("Cannot determine oponent's play"),
    };

    let desired_outcome = match inputs[1] {
        "X" => PlayResult::Loss,
        "Y" => PlayResult::Tie,
        "Z" => PlayResult::Win,
        _ => panic!("Cannot determine desired outcome"),
    };

    (oponents_play, desired_outcome)
}

fn main() -> std::io::Result<()> {
    let mut total_score_a: u32 = 0;
    let mut total_score_b: u32 = 0;

    let file = File::open("input.txt")?;
    let buf_reader = BufReader::new(file);
    let lines = buf_reader.lines();

    for line in lines.flatten() {
        let (my_play, oponents_play) = parse_line_a(&line);
        let round_result = my_play.vs(&oponents_play);
        total_score_a += my_play.value() + round_result.value();

        let (oponents_play, desired_outcome) = parse_line_b(&line);
        let my_play = match desired_outcome {
            PlayResult::Win => oponents_play.loses_vs(),
            PlayResult::Loss => oponents_play.wins_vs(),
            PlayResult::Tie => oponents_play,
        };

        total_score_b += my_play.value() + desired_outcome.value();
    }

    println!("solution A: {}", total_score_a);
    println!("solution B: {}", total_score_b);

    Ok(())
}
